export const environment = {
  production: true,
  dataServer: 'http://localhost:3000',
  fileServer: 'http://localhost:3000',
  dataServerPrefix: 'api',
  fileServerPrefix: 'api',
  ws: 'ws://localhost:3000'
};
