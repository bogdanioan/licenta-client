import { environment } from 'src/environments/environment';
const dataPrefix = environment.dataServerPrefix;
const filePrefix = environment.fileServerPrefix;
export const urls = {
    login: [dataPrefix, 'auth', 'login'].join('/'),
    saveImage: [filePrefix, 'file'].join('/'),
    getImage: [filePrefix, 'file'].join('/'),
    getDetails: [filePrefix, 'file'].join('/')
};
