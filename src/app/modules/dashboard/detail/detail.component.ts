import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StatesService } from 'src/app/states.service';
import { Subscription } from 'rxjs';
import { FileService } from '../services/file.service';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit, AfterViewInit, OnDestroy {
  subscriptions: Subscription[] = [];
  src = null;
  text = '';
  textGoogle = '';
  constructor(private domSanitazer: DomSanitizer,
              private statesService: StatesService,
              private fileService: FileService,
              private router: Router,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.subscriptions.push(this.statesService.getRefresh().subscribe(payload => {
      if (payload) {
        this.router.navigate(['../../'], { relativeTo: this.activatedRoute });
      }
    }));

    this.subscriptions.push(this.activatedRoute.paramMap.subscribe(params => {
      const id = params.get('id');
      this.subscriptions.push(this.fileService.getImage(id).subscribe((payload: any) => {
        this.src = this.domSanitazer.bypassSecurityTrustUrl(`data:image/jpeg;base64,${payload.img}`);
      },
        err => this.router.navigate(['../../'], { relativeTo: this.activatedRoute })
      ));
      this.subscriptions.push(this.fileService.getDetails(id).subscribe((payload: any) => {
        this.text = payload.text;
        this.textGoogle = payload.textGoogle;
      },
        err => this.router.navigate(['../../'], { relativeTo: this.activatedRoute })));
    }));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }
}
