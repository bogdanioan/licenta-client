import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { FileDropModule } from 'ngx-file-drop';
import { InputTextModule } from 'primeng/inputtext';
import { AceEditorModule } from 'ng2-ace-editor';
import { HttpClientModule } from '@angular/common/http';
import { ButtonModule } from 'primeng/button';
import { FormsModule } from '@angular/forms';
import { FileService } from './services/file.service';
import { NotificationService } from './services/notification.service';
import { DetailComponent } from './detail/detail.component';

@NgModule({
  declarations: [DashboardComponent, DetailComponent],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    InputTextModule,
    FileDropModule,
    ButtonModule,
    HttpClientModule,
    AceEditorModule,
    FormsModule,
  ],
  providers: [FileService, NotificationService]
})
export class DashboardModule { }
