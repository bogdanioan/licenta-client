import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { urls } from 'src/app/utils/urls';

@Injectable()
export class FileService {

  constructor(private http: HttpClient) { }

  saveImage(image, imageName) {
    const formData = {
      file: image,
      fileName: imageName,
    };
    return this.http.post(urls.saveImage, formData);
  }
  saveImages(images, imageName) {
    const formData = {
      files: images,
      fileName: imageName,
    };
    return this.http.post(urls.saveImage, formData);
  }

  getImage(id) {
    return this.http.get(`${urls.getImage}?img=${id}`);
  }

  getDetails(id) {
    return this.http.get(`${urls.getDetails}?id=${id}`);
  }
}
