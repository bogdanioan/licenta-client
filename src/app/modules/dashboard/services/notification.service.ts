import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class NotificationService {
  ws;
  loadingSubject = new BehaviorSubject(null);

  constructor() {
    this.listenChanges();
  }
  listenChanges(): any {
    this.ws = new WebSocket(environment.ws);

    this.ws.onopen = () => {
      console.log('Connected...');
    };

    this.ws.onclose = () => {
      console.log('Connection closed...');
      this.listenChanges();
    };


    this.ws.onmessage = (evt) => {
      const data = JSON.parse(evt.data);
      this.loadingSubject.next(data);
    };
  }
  getLoadingNotifications() {
    return this.loadingSubject;
  }
}
