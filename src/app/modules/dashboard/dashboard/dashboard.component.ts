import { Component, OnInit, OnDestroy } from '@angular/core';
import { UploadEvent, UploadFile, FileSystemFileEntry, FileSystemDirectoryEntry } from 'ngx-file-drop';
import { FileService } from '../services/file.service';
import { NotificationService } from '../services/notification.service';
import { Subscription } from 'rxjs';
import { constants } from 'src/app/utils/constants';
import { StatesService } from 'src/app/states.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {
  fileName;
  files: UploadFile[] = [];
  src = null;
  isLoading = false;
  message = constants.defaultLoadingMessage;

  text = '';
  textGoogle = '';
  shareLink = null;

  subscriptions: Subscription[] = [];
  constructor(private fileService: FileService,
              private notificationService: NotificationService,
              private statesService: StatesService) { }

  ngOnInit() {
    this.subscriptions.push(this.notificationService.getLoadingNotifications().subscribe((payload: any) => {
      if (payload && this.fileName === payload.fileName && payload.code) {
        this.textGoogle = payload.code;
      } else if (payload && this.fileName === payload.fileName) {
        this.message = payload.status;
      }
    }));
    this.subscriptions.push(this.statesService.getRefresh().subscribe(payload => {
      if (payload) {
        this.refresh();
      }
    }));
  }

  refresh() {
    this.src = null;
    this.text = '';
    this.textGoogle = '';
    this.isLoading = false;
    this.shareLink = null;
  }


  public dropped(event: UploadEvent) {
    this.refresh();
    this.files = event.files;
    const droppedFile = event.files[0];

    if (droppedFile.fileEntry.isFile) {
      const fileEntry = droppedFile.fileEntry as FileSystemFileEntry;
      fileEntry.file((file: File) => {
        this.fileName = file.name;
        const reader = new FileReader();
        reader.onload = e => this.src = reader.result;
        reader.readAsDataURL(file);
      });
    } else {
      const fileEntry = droppedFile.fileEntry as FileSystemDirectoryEntry;
    }
  }

  buttonFileUpload(event) {
    this.refresh();
    const file = event.item(0);
    this.fileName = file.name;
    const reader = new FileReader();
    reader.onload = e => this.src = reader.result;
    reader.readAsDataURL(file);
  }

  sendData() {
    const url = `${window.location.href}/details`;
    this.isLoading = true;
    this.fileName = `${this.fileName}-${Date.now()}`;
    this.subscriptions.push(this.fileService.saveImage(this.src, this.fileName).subscribe((res: any) => {
      this.text = res.text;
      this.shareLink = `${url}/${res.reference}`;
      this.isLoading = false;
      this.message = constants.defaultLoadingMessage;
    }));
  }

  copyToClipboard() {
    const dummy = document.createElement('textarea');
    document.body.appendChild(dummy);
    dummy.value = this.text;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
  }

  copyGoogleToClipboard() {
    const dummy = document.createElement('textarea');
    document.body.appendChild(dummy);
    dummy.value = this.textGoogle;
    dummy.select();
    document.execCommand('copy');
    document.body.removeChild(dummy);
  }

  copyLink(target) {
    target.select();
    document.execCommand('copy');
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
    this.statesService.setRefresh(false);
  }
}
