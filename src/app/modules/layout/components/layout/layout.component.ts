import { Component, OnInit } from '@angular/core';
import { StatesService } from 'src/app/states.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  constructor(private statesService: StatesService) { }

  ngOnInit() {
  }

  refresh() {
    this.statesService.setRefresh(true);
  }

}
