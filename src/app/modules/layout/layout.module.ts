import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './components/layout/layout.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { MenuModule } from 'primeng/menu';
import { ChartModule } from 'primeng/chart';
import { SidebarModule } from 'primeng/sidebar';
@NgModule({
  declarations: [LayoutComponent],
  imports: [
    CommonModule,
    LayoutRoutingModule,
    ChartModule,
    MenuModule,
    SidebarModule
  ],
  providers: []
})
export class LayoutModule { }
