import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class StatesService {
  refresh = new BehaviorSubject<boolean>(false);
  constructor() { }

  setRefresh(value) {
    this.refresh.next(value);
  }
  getRefresh() {
    return this.refresh;
  }
}
